
# Generate Tarballs

[TOC]

## About
For my asterosismic grid, I will produce more than 37 000 000 [GYRE](https://bitbucket.org/rhdtownsend/gyre/wiki/Home "Click to open") HDF5 files. In order to keep the number of output files per directory reasonalbe, I need to bundle the HDF5 files as compressed TAR files, store and transfer them to our local host. This routine takes a great care of the file and directory structuring, and creates tarballs with a *parallel do*, so that each core on a machine will create TAR files on one directory, where the number of directories is much greater than the number of available cores on the compute node.

## Usage
Simply, open the `make-tarball.sh` file, and modify the relevant *paths* and *constants* in the bottom of the script. Then, you simply call
```
bash make-tarball.sh
```

## Inputs
There are two directories included with zero-sized sample .h5 files, which just demonstrate how this script can work in general. One just needs to grab the script, and modify the paths to deploy it on any cluster/supercomputer to create TAR files with thosands of files in each, iteratively.

## Settings