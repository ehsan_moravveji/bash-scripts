#! /bin/bash

###############################################################################
#
# D E F I N I T I O N    O F   P A T H S   A N D   F I L E S
#
export suffix='tar'
export which_eta=00.00
export dir_tar=/home/user/the_tar_files
export dir_extract=/home/user/extract_files
export strip_components=7

###############################################################################
#
# F U N C T I O N    D E F I N I T I O N
#

function get_work_files {
  # purpose: Grab the files to work on inside the work_dir with the given extension suffix,
  #          provided they have $which_eta in their filenames, e.g. if which_eta is set above 
  #          to 12.34, then all files like "M*-eta12.34*" will be grabbed
  #          Note that we apply the wildcard "*" with the search regex
  # use:     get_work_files work_dir suffix result
  # args:    1. The work directory where the target files reside
  #          2. The extention of the files to create checksum, e.g. fits, h5, dat, txt, etc.
  #             Note that suffix cannot have the dot delimiter "." in it
  #          3. result
  # result:  The list of target files to work with

  local path=$1
  local sfx=$2
  local _res=$3
  
  # Ensure the suffix does not contain "."
  if [[ $suffix == *.* ]]; then 
    echo 'Error: get_work_files: suffix cannot contain "."'
    exit 1
  fi

  local res=$(ls $path/*-eta$which_eta.$sfx)
  eval $_res="'$res'"
}

function gen_M_eta_directory {
  # purpose: Create a directory from star mass, and a subdirectory from rotation rate eta.
  #          The mass and eta are both grabbed from the input name of the tarball
  #          E.g. from the file "/home/user/myfiles/M12.345-eta01.23.tar", we extract
  #          "M12.345/eta01.23"
  #          The whole trick is that
  # use:     gen_M_eta_directory tar_file result
  # args:    1. the full path to the tarball to be extracted
  #          2. result
  # result:  The mass folder + eta subfolder extracted from filename.

  local fname=$1
  local _res=$2

  # trim off the trailing path
  local filename=$(basename $fname)
  # trim off the extention
  local len=$((${#suffix} + 1))
  local corename=${filename::-len}

  # relapce - with / to convert the core name into directory name
  local res=${corename//-/\/}
  eval $_res="'$res'"

}

###############################################################################
#
# E X E C U T E
#

# Get the target files
get_work_files $dir_tar $suffix tar_files
if [ $? != 0 ]; then 
  echo 'Error: get_work_files failed'
  exit 1
fi

# Find the number of tar files
temp=($tar_files)
num_tar=${#temp[@]}
echo 'Found' $num_tar \"$suffix\" 'files in the folder:' $dir_tar

# Iterate over the files, create proper extraction folder name, and extrac them there
for tar_file in $tar_files; do 
  # echo $tar_file
  gen_M_eta_directory $tar_file dir_mass_eta
  export dir_untar=$dir_extract/$dir_mass_eta
  mkdir -p $dir_untar

  tar -xf $tar_file --strip-components=$strip_components -C $dir_untar
  echo "Success:" $tar_file 
  if [ $? != 0 ]; then
     echo 'Error: could not extract' $tar_file
     exit 1
  fi
  exit 0
done

###############################################################################

