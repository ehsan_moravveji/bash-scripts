#! /bin/bash

###############################################################################
#
# D E F I N I T I O N    O F   P A T H S   A N D   F I L E S   A N D    C O N S T A N T S
#

# Specify which rotation rate (eta)
export eta=00.00
export str_eta=eta$eta

# Paths and directories
export WORKDIR=$PBS_O_WORKDIR
export dir_repos=`pwd`
export dir_tar=$dir_repos/tarballs

mkdir -p $dir_tar

export max_num_simult_proc=4 # set this reasonably less than the number of available cores on the machine

###############################################################################
#
# F U N C T I O N   D E F I N I T I O N
#

function main {

  local N_proc=0
  
  # store all relevant directory names in $dir_names
  get_directories $dir_repos dir_names

  # iterate over each folder, and find relevant files in each directory
  # with the correct eta in its filename
  for dir_M in $dir_names; do

      echo Working on $dir_M 

      # export str_mass
      # get_mass_string $dir_M $dir_repos str_mass
      get_mass_string $dir_M str_mass

      # construct the output tar name based on the mass and eta
      set_tar_name $str_mass tar_name

      # construct the target gyre_out folder 
      get_gyre_out_dir $dir_M gyre_out_dir

      # create the tarball now
      create_tar $tar_name $gyre_out_dir &

      N_proc=$[$N_proc+1]
      if [ $N_proc -ge $max_num_simult_proc ]; then 
         wait 
         N_proc=0
      fi
  done
  wait 
}

function create_tar {
  # purpose: create a tarball from files in a specified directory
  #          The newer version excludes the trailing directory tree
  # call syntax: create_tar tar_name which_dir
  # result: None

  local tar_filename=$1
  local which_dir=$2

  # tar -cf $tar_filename -P $which_dir/*$str_eta.h5
  tar -cf $tar_filename -C $which_dir -P $which_dir/*$str_eta.h5 .
  if [ $? != 0 ]; then 
     echo 'Error: create_tar failed'
     exit 1
  fi
}

function set_tar_name {
  # call syntax set_tar_name M12.345 result 
  # result: the full path to the .tar file

  local M=$1
  local _res=$2
  local tar_filename=$dir_tar/$M-$str_eta.tar
  eval  $_res="'$tar_filename'"
}

function get_gyre_out_dir {
  # purpose: generate the full path to the gyre_out directory where .h5 files exist
  # call syntax: get_gyre_out_dir dir_M result
  # result: full path to one specific gyre_out directory

  local the_dir_M=$1
  local _res=$2
  local res=$1/gyre_out
  eval  $_res="'$res'"
}

function get_mass_string {
  # purpose: to extract the mass string from the directory name
  # call syntax: get_mass_string full_string_to_clip result
  # result: str_mass is returned, e.g. M12.345

  local full_path=$1
  local _res=$2

  local trailing=$(basename "${full_path}") 
  eval  $_res="'$trailing'"

  # local _res=$3
  # local the_full_string=$1
  # local string_to_remove=$2
  # local position=${#string_to_remove}
  # position=$[$position+1]
  # local res=${the_full_string:position}
  # eval  $_res="'$res'" 
}

function get_directories {
  # purpose: Get the list of directories whose name start with M, e.g. M01.400, M01.425 ...
  #          If no desired directories are found, it exits with error code 1.
  # use:     get_directories path result
  # args:    1. The full path to the repository to search for folders starting with "M"
  #          2. result
  # result: list of directories inside dir_repos which start with M

  local path=$1
  local _res=$2
  local res=$(ls -d $path/M*)
  if [ $? != 0 ]; then 
     echo 'Error: get_directories:' $path 'has no subfolder starting with character "M"'
     exit 1
  fi
  eval $_res='"$res"'
}

function get_num_subdirectory_levels {
  # call syntax: get_num_subdirectory_levels $gyre_out_dir result
  # result: num_subdirectory_levels

  slash="/"
  local _res=$2
  local number_of_folders=$(grep -o "$slash" <<< "$1" | wc -l)
  local num_subdirectory_levels=$[$number_of_folders+1] # extra level added for "gyre_out" folder
  eval $_res="'$num_subdirectory_levels'"
}

function get_num_avail_cores {
  # purpose: Get the max number of available cores on the current workstation
  # use:     get_num_avail_cores result
  # args:    1. result
  # result:  Integer passing the max number of cores on the machine

  local _res=$1
  local os=`uname`
  if [ $os = 'Linux' ]; then 
    local res=$(nproc all)
  elif [ $os = 'Darwin' ]; then 
    local res=$(getconf _NPROCESSORS_ONLN)
  else
    echo 'Error: get_num_avail_cores: This operating system is not supported yet'
    exit 1
  fi
  eval $_res="'$res'"
}

###############################################################################
#
# E X E C U T E
#

get_num_avail_cores num_avail_cores
if [ $max_num_simult_proc -gt $num_avail_cores ]; then 
  echo 'Lowering the number of requested threads from' $max_num_simult_proc 'to' $num_avail_cores
  max_num_simult_proc=$num_avail_cores
fi

# Do the job here
main

echo All tarballs in $dir_tar $(ls $dir_tar)
echo The size of $dir_tar in MB $(du -hs $dir_tar)
