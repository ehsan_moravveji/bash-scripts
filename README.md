# My Bash Scripts 

[TOC]

## What is it about?
I am putting some of my bash scripts online, first of all for myself to easily 
fetch, and secondly for any other interested person to take a look, grab, and 
adapt to his/her own applications.

## Projects
 * [Parallel Do](./bash-parallel-do/README.md "Execute series of independent jobs in parallel")
 * [Make Tarballs](./bash-make-tarball/README.md "Make tar bundles")
 * [HDF5 File Integrity Check](./bash-check-HDF5-integrity/README.md "Check HDF files")
 * [Generate Checksum](./bash-checksum/README.md "Make md5 checksum table for many files")

## Who is this for?
Myself, at first hand, since I need to reuse these scripts for different applications.
However, any of my graduate students or any individual is more than welcome to exploit
these bash scripts, and use them as prototypes for their own problems.

## Documentation
Each of the Bash scripts in any of the projects are documented. In the preamble of each
funtion, four items are fully addressed:
(1) the **purpose** of that specific function, and why it can be useful, 
(2) the **use** of a function, as a syntactical reference, 
(3) **args** the meaning of each input and output argument to the function call, and
(4) the **result** or the return output from each function.

## Disclaimer
This repository is copyrighted to Ehsan Moravveji, and is freely available through bitbucket.
It comes without any warranty; so, the users are encouraged to read the documentation of each 
subroutine and also the source code before using any of the tools.
