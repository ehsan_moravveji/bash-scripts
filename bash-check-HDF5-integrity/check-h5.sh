#! /bin/bash

###############################################################################
# N O T E S
# 
# The h5check command which we use is documented in the following pdf file:
# https://support.hdfgroup.org/ftp/HDF5/tools/h5check/src/unpacked/doc/h5check.pdf

###############################################################################
#
# D E F I N I T I O N    O F   P A T H S   A N D   F I L E S
#
export dir_repos=~/Desktop
export log_file=log-bad-h5.txt

###############################################################################
#
# F U N C T I O N    D E F I N I T I O N
#

function ensure_h5check_installed {
  # purpose: Before executing the script, we must ensure that the h5check is available
  #          in the command line. If not installed, this function returns exit code "1".
  # use:     ensure_h5check_installed
  # args:    None
  # result:  None
  local version_number=`h5check --version`
  local status=$?
  if [ $status != 0 ]; then 
     echo 'Error: h5check not installed.'
     echo '       Refer to https://support.hdfgroup.org/products/hdf5_tools/h5check.html'
     exit 1
  fi
}

function get_directories {
  # purpose: Get the list of directories whose name start with M, e.g. M01.400, M01.425 ...
  #          If no desired directories are found, it exits with error code 1.
  # use:     get_directories path result
  # args:    1. The full path to the repository to search for folders starting with "M"
  #          2. result
  # result: list of directories inside dir_repos which start with M

  local path=$1
  local _res=$2
  local res=$(ls -d $path/M*)
  if [ $? != 0 ]; then 
     echo 'Error: get_directories:' $path 'has no subfolder starting with character "M"'
     exit 1
  fi
  eval $_res='"$res"'
}

function do_h5check {
  # purpose: Execute the h5check command, and return the exit code.
  #          We use the "terse" mode (--verbose=0), for fast querying the file integrity.
  #          The return codes have the following meanings:
  #          - 0: Succeeded.
  #          - 1: Command failures such as argument errors.
  #          - 2: Format compliance errors found.
  # use:     do_h5check filename result
  # args:    1. The full path to the HDF5 (.h5) file to inspect
  #          2. result
  # result:  The return code from h5check command; integer either 0, 1 or 2

  local filename=$1
  local _res=$2
  h5check --verbose=0 $filename
  local res=$?
  eval $_res="'$res'"
}

function log_bad_file {
  # purpose: Append the full path to the corrupted HDF5 file 
  # use:     log_bad_file logger filename
  # args:    1. The full path to the logger file 
  #          2. The full path to the corrupted .h5 file 
  # result:  None

  local logger=$1
  local filename=$2
  echo $filename | tee -a $logger
}

###############################################################################
#
# E X E C U T E
#

# Is h5check installed?
ensure_h5check_installed

# Get mass directories, starting with "M"
get_directories $dir_repos dir_M_names

# iterate over each folder 
for dir_M in $dir_M_names; do 

  # construct the search folder 
  target_dir=$dir_M/gyre_out

  # find *.h5 files in each directory
  target_h5_files=`ls $target_dir/*.h5`
  if [ $? != 0 ]; then 
     echo 'Error: Found no .h5 files in' $target_dir
     exit 1
  fi 

  # iterate over each file, and execute h5check
  for h5_file in $target_h5_files; do 
      
      do_h5check $h5_file err_code

      if [ $err_code != 0 ]; then 
         log_bad_file $log_file $h5_file
      fi 
  done

done 

