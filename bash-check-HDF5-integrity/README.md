
# Check HDF5 File Integrity

[TOC]

## About

Our asteroseismic grids for forward modeling typically consists of an order of few million (e.g. 30) HDF5 files, which are generated on a cluster/supercomputer using tens to few hundreds of CPUs at the same time. Thus, it is possible that due to unanticipated I/O interactions, one to many files are written inappropriately on the disk, and become corrupted. This must be checked before taking further steps (compressing, transfering, uncompressing, creating database, and post-processing). The `h5check` offers three possible integrity checks, which we only restrict ourselves to the *terse* check, because dealing with millions of HDF5 files can be quite time consuming.  

This script checks the integrity of the HDF5 binary files, *without reading the file content*, and without any need to use the relevant HDF libraries. The relevant command is **`h5check`**, and this tool is described in [h5check](https://support.hdfgroup.org/products/hdf5_tools/h5check.html "The HDFGroup"). The documentation can also be found as a PDF file in the following [URL](https://support.hdfgroup.org/ftp/HDF5/tools/h5check/src/unpacked/doc/h5check.pdf "h5check docs"). 

## Calling 

Basically, one needs to execute the script from the command line like the following

```$> bash check-h5.sh```

## Settings

In order to adapt the script to your own problem, you need to configure the following static paths on top of the script.

  * **`export dir_repos=/home/user/grid-repository`**

    The `dir_repos` specifies where the asteroseismic grid lies. Inside the repository, all folders start with the initial mass of the targets, e.g. M12.345.

  * **`export log_file=log-bad-h5.txt`**

    The name of the logger file which will contain the full path to all files which `h5check` returned a non-zero exit code. Such files, if present, are considered corrupted, and need to be recalculated using GYRE.




