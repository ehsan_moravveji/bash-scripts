#! /bin/bash

###############################################################################
#
# D E F I N I T I O N    O F   P A T H S   A N D   F I L E S
#
export suffix='SFX'
export dir_files=/home/me/path_files
export dir_save=/home/me/path_save
export output_file='checksum-for-'$suffix'.chk'


# Set the correct checksum command, since it depends on the platform
export os=`uname`
if [ "$os" != "Linux" ] && [ "$os" != "Darwin" ]; then 
  echo 'Error'
  exit 1
fi

###############################################################################
#
# F U N C T I O N    D E F I N I T I O N
#
function get_work_files {
  # purpose: Grab ALL files to work on inside the work_dir with the given extension suffix
  #          Note that we apply the wildcard "*" with the search regex
  # use:     get_work_files work_dir suffix result
  # args:    1. The work directory where the target files reside
  #          2. The extention of the files to create checksum, e.g. fits, h5, dat, txt, etc.
  #             Note that suffix cannot have the dot delimiter "." in it
  #          3. result
  # result:  The list of target files to work with

  local path=$1
  local sfx=$2
  local _res=$3
  
  # Ensure the suffix does not contain "."
  if [[ $suffix == *.* ]]; then 
    echo 'Error: get_work_files: suffix cannot contain "."'
    exit 1
  fi

  local res=$(ls $path/*.$sfx)
  eval $_res="'$res'"
}

function count_number_of_files {
  # purpose: Count how many space-separated tokens are present in a bash variable
  # use:     count_number_of_files var result
  # args:    1. The variable to count the number of tokens of
  #          2. result
  # result:  integer, passing the number of space-delimited tokens in the variable "var"

  local var=$1
  local _res=$2
  var=( $var )
  local res=${#var[@]}
  eval $_res="'$res'"
}

function get_checksum {
  # purpose: Create a checksum string for an input file
  # use:     get_checksum filename result
  # args:    1. The full path to the target filename 
  #          2. result
  # result:  The checksum string

  local filename=$1
  local _res=$2

  if [ "$os" = "Linux" ]; then 
    local res=$(md5sum $filename)
  elif [ "$os" = "Darwin" ]; then 
    local res=$(md5 -q $filename)
  fi 
  eval $_res="'$res'"
}

function rfind {
  # purpose: find the right-most position of a character in a string
  # use:     rfind string character result
  # args:    1. string to search the right-most position of the character
  #          2. character to search for, e.g. "/" or "."
  #          3. result
  # result:  integer >0, if successful, and 0 if failed (character not in string)

  local string=$1
  local char=$2
  local _res=$3
  local pos=0
  for (( counter=0; counter<${#string}; counter++ )); do 
      s=${string:$counter:1}
      # echo $counter $s 
      if [ $s = $char ]; then 
        pos=$counter
      fi
  done
  eval $_res="'$pos'"
}

function append_md5_to_ascii {
  # purpose: Add the file checksum to a log file
  # use:     append_md5_to_ascii filename md5 log_file
  # args:    1. The filename which the checksum is available
  #          2. The checksum fingerprint
  #          3. The full path to the logger file
  # result:  None

  local filename=$1
  local chksum=$2
  local log_file=$3
  # echo $filename $chksum | tee -a $log_file
  echo $filename $chksum >> $log_file
}

###############################################################################
#
# E X E C U T E
#

# Create an empty output file
if [ ! -f $output_file ]; then 
  echo 
  echo 'Creating the output file $output_file'
  echo 
  touch $output_file
fi

# Find all the target files to work with
get_work_files $dir_files $suffix work_files
if [ $? != 0 ]; then 
  echo 'Error: get_work_files failed'
  exit 1
fi

# Count the number of files (here tokens) in the variable
temp=( $work_files )
num_files=${#temp[@]}

for w in $work_files; do
  
  get_checksum $w str_md5
  if [ $? != 0 ]; then 
    echo 'Error: get_checksum failed'
    exit 1
  fi

  # Get the file core name
  rfind $w "/" pos_slash
  core_name=${w:pos_slash+1}

  append_md5_to_ascii $core_name $str_md5 $output_file

done

echo 
echo 'Checksum Summary:'
echo 'checksum for' $num_files '*.$suffix' files 'in' $dir_files 'saved in' $output_file
echo
cat $output_file
exit 0
