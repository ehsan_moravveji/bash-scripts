
# Create Checksum 

[TOC]

## About

When transferring large amount of data through network, it is important to check the file integrity at the destination. For that it is critical to get the file fingerprint at the host, and ensure that it matches the file fingerprint at the destination after the transfer. In such situations, the `checksum` is your friend. On the Linux-based operating systems, the relevant command is `md5sum`, while on the Mac OS (Darwin) one must execute the `md5` command. This script distinguishes these the OS and uses the proper command.

## Calling

To run the script, you must only adapt the path settings on top of the bash script, and execute the following command in the terminal

```bash create-checksum.sh```

## Settings

To adapt the script to your own application, the following few options must be tweaked in the preamble of the script.
Below, I explain their meanings
  
  * **`export suffix='SFX'`**

    The extension of the files that we will create a checksum for. Make sure you do not include a leading ".", because it is appended internally. As an example, ".SFX" is not accepted.

  * **`export dir_files=/home/me/path_files`**

    The full path to the directory where the target files reside.

  * **`export dir_save=/home/me/path_save`**   

    The checksum file will be directed to this folder.

  * **`export output_file=$dir_save/result.chk`** 

    The full path to the output file which stores the md5 fingerprint of files, one per line. The file is simply formated as two columns separated by a single space, with the first column giving the name of the file, and the second column its corresponding fingerprint.
 
