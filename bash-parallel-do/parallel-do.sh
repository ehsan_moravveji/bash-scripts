#! /bin/bash

###############################################################################
#
# D E F I N I T I O N    O F   P A T H S   A N D   F I L E S
#

export reference_namelist=reference-gyre.nmlst # compatible with GYRE version 4.4
export dir_inputs=inputs  # all-inputs
export dir_outputs=outputs 
export dir_namelist=namelists 

mkdir -p $dir_outputs $dir_namelist

export namelist_prefix=namelist 
export namelist_suffix=txt 

export max_simultan_proc=4
export OMP_NUM_THREADS=1 # use only 1 core per task; do not change this, even if your workstation has many cores
export gyre=$GYRE_DIR/bin/gyre_ad

###############################################################################
#
# F U N C T I O N   D E F I N I T I O N
#

function welcome {
  # purpose: Print out the welcome message on the STDOUT
  # use:     welcome
  # args:    None
  # result:  None

  echo 
  echo "Parallel Do."
  echo "Now using" $max_simultan_proc "simultaneous threads for all jobs"
  echo 
}

function get_num_avail_cores {
  # purpose: Get the max number of available cores on the current workstation
  # use:     get_num_avail_cores result
  # args:    1. result
  # result:  Integer passing the max number of cores on the machine

  local _res=$1
  local os=`uname`
  if [ $os = 'Linux' ]; then 
    local res=$(nproc all)
  elif [ $os = 'Darwin' ]; then 
    local res=$(getconf _NPROCESSORS_ONLN)
  else
    echo 'Error: get_num_avail_cores: This operating system is not supported yet'
    exit 1
  fi
  eval $_res="'$res'"
}

function get_input_files {
  # purpose: find and return all .gyre input files in a given folder
  # use:     get_input_files /home/me/stuff/gyre-files result
  # args:    1: directory where inputs are stored
  #          2. result
  # result:  list of all files matching *.gyre search criterion

  local _res=$2
  local res=$(ls $1/*.gyre)
  eval $_res="'$res'"
}

function generate_namelist_filename {
  # purpose: generate a unique name for the GYRE input namelist file, based on the counter
  #          also, prepend the dir_namelist to the namelist filename
  # use:     generate_namelist_filename dir_namelists prefix counter suffix result
  # args:    1. the full path to the folder where namlists are stored
  #          2. the namelist prefix name
  #          3. the running integer counter 
  #          4. the namelist suffix name 
  #          5. the result
  # result:  export namelist

  local _res=$5
  local res=$1/$2-$3.$4
  eval $_res="'$res'"
}

function get_string_length {
  # purpose: get the length of a string 
  # use:     get_string_length string length
  # args:    1. string whose length we're interested in
  #          2. result
  # result:  integer that enumerates the length of the string in units of characters

  local string=$1 
  local _res=$2
  local res=${#string}
  eval $_res="'$res'"
}

function match_length_substring_in_string {
  # purpose: get the length of the substring that matches the string
  # use:     match_length_substring_in_string string substring result
  # args:    1. The full string to that is looked into
  #          2. The substring to match. It can be a regular expression
  #          3. result
  # result:  integer; the length of the matching string 

  local string=$1
  local substring=$2
  local _res=$3
  # local res=`expr match "$string" $substring`
  local res=${string%%substring}
  eval $_res="'$res'"
}

function remove_leading_substring_from_string {
  # purpose: To drop a fraction of directory tree from a full path, e.g. exclude "/home" from "/home/me"
  # use:     remove_leading_substring_from_string full_path exclude result
  # args:    1. The full path which we are going to trim off
  #          2. The substring pattern that we exclude
  #          3. result
  # result:  the remaining part of the path

  local path=$1
  local excl=$2
  local _res=$3
  local pos=${#excl}
  pos=$[$pos+1]         # extra character to remove "/"
  local res=${path:pos}
  eval $_res="'$res'"
}

function remove_trailing_substring_from_string {
  # purpose: To drop trailing part of a path/filename, e.g. exclude "me" from "/home/me"
  # use:     remove_trailing_substring_from_string 
  local string=$1
  local substring=$2
  local _res=$3
  get_string_length $string len_str 
  get_string_length $substring len_substr
  len_trim=$(($len_str-$len_substr-1)) 
  local res=${string:0:$len_trim}
  eval $_res="'$res'"
}

function get_core_filename {
  # purpose: strip off a filename from the leading directory name, and trailing extension
  #          note that the extension must not have "." in it
  #          E.g. we get "file" from "/path/file.extension"
  # use:     get_core_filename string leading_strings extension result
  # args:    1. the full path to be stripped off of its leading and trailing parts
  #          2. the full directory name to be trimmed off first
  #          3. the extension to be trimmed off, e.g. "gyre", "h5", "txt", ...
  #          4. result
  # result:  the file core name

  local string=$1
  local leading_strings=$2
  local extension=$3
  local _res=$4
  remove_leading_substring_from_string $string $leading_strings leading
  remove_trailing_substring_from_string $leading $extension trailing 
  eval $_res="'$trailing'"
}

function get_h5_filename {
  # purpose: construct the name of the output .h5 filename based on the input filename
  # use:     get_h5_filename input_filename leading_path extension result
  # args:    1. the full path to the input filename
  #          2. the full path to the leading directory to be trimmed off
  #          3. the file extension to be trimmed off, without ".", e.g. "gyre"
  #          4. result
  # result:  full path of the output filename to be dumped into the GYRE namelist

  local string=$1
  local substring=$2
  local extension=$3
  local _res=$4
  get_core_filename $string $substring $extension core_filename
  local h5_filename=$dir_outputs/$core_filename.h5
  eval $_res="'$h5_filename'"
}

function replace_slash {
  # purpose: replace "/" in the path with "\/" to be used for sed interpreter
  # use:     replace_slash string result
  # args:    1. the string (path) that contains any occurance of "/"
  #          2. result
  # result:  the same string, with "/" replaced with "\/"

  local string=$1
  local _res=$2
  local res=${string//\//\\/}
  eval $_res="'$res'"
}

function change_gyre_inlist_one_line {
  # purpose: One can basically modify any line in the GYRE namelist, by providing 
  #          the left-hand-side of the item, and the full line to be substituted
  # use:     change_gyre_inlist_one_line filename old_item new_item 
  # args:    1. the full path to the namelist file to be modified
  #          2. the item to be changed. E.g. "summary_file="
  #          3. the new item to be replaced. E.g. "summary_file='output.h5'"
  # result:  nothing is returned

  local target_file=$1
  local item=$2
  local new=$3
  local sed_arg='s/'$item'/'$new'/g'
  sed -i -e $sed_arg $target_file
}

function call_gyre {
  # purpose: Call GYRE
  # use:     call_gyre namelist
  # args:    1. full path to the namelist file
  # result:  None

  $gyre $1
}

###############################################################################
#
# E X E C U T E
#

  welcome
  get_num_avail_cores num_avail_cores 
  if [ $max_simultan_proc -gt $num_avail_cores ]; then
     echo
     echo 'Lowering the number of working cores from' $max_simultan_proc 'to' $num_avail_cores
     echo 
     # eval $max_simultan_proc="'$num_avail_cores'"
     max_simultan_proc=$num_avail_cores
  fi
  N_proc=0

  # Find all *.gyre files in dir_inputs, and export input_files
  get_input_files $dir_inputs input_files

  counter=0
  for input in $input_files; do 

    # Set a counter to keep track of the calls
    let counter++ 

    # echo $counter $input

    # # Generate the name of the namelist file based on the iteration number
    generate_namelist_filename $dir_namelist $namelist_prefix $counter $namelist_suffix namelist

    # # Copy the reference inlist to the namelist folder for further modification and use
    cp $reference_namelist $namelist 

    # # Construct the output filename based on the dir_outputs and the input filename
    get_h5_filename $input $dir_inputs gyre h5_file

    # Change the full path to input .gyre file in the inlist file
    replace_slash $input input_for_sed 
    change_gyre_inlist_one_line $namelist 'input_file=' "file='$input_for_sed'" 

    # Change the full path to the output .h5 file in the inlist file
    replace_slash $h5_file h5_file_for_sed
    change_gyre_inlist_one_line $namelist 'summary_file=' "summary_file='$h5_file_for_sed'"
    
    # Info 
    echo $counter. Working on: $input

    # Call GYRE for one specific namelist
    call_gyre $namelist > /dev/null &

    N_proc=$[$N_proc+1]
    if [ $N_proc -ge $max_simultan_proc ]; then 
       wait 
       N_proc=0
    fi

    # # Remove the namelist after GYRE has successfully finished
    # remove_namelist $namelist

  done
wait  # to ensure all processes finish before exiting

echo
echo $(ls $dir_outputs/*.h5 | wc -l) .h5 files found in $dir_outputs
echo

exit 0

###############################################################################

