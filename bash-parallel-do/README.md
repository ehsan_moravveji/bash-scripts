
# Parallel Do

[TOC]

## About 

If you have a N number of *independent* jobs, and a compute node/desktop/laptop 
with M cores (where M>1), then, you may wish to deploy each task/job on one core. 
This is an approach to basically *parallelize the problem*. This approach is useful
when the number of tasks reasonably exceeds the number of available cores (i.e. N>>M),
in addition to when the time spent on each task is reasonably long. The `parallel-do.sh`
script is one working example.

## Motivation

I normally have access to a supercomputer or a local computing cluster with hundreds
of available computing cores. However, there are situations when I want to spend zero
seconds waiting in a queue, for the purpose of *prototyping*. Or, if I am on a work-related
trip, and have no easy access to internet/cluster, I can still work very efficiently,
by parallelizing my problem on a local laptop.

## Prerequisites

This specific example calls the pulsation code [GYRE](https://bitbucket.org/rhdtownsend/gyre/wiki/Home "Click to open") to iterate over a number of input files, to compute their adiabatic pulsation frequencies, and to store
the results in a user-defined directory. Therefore, GYRE must be already installed on your workstation.
Furthermore, to try using this script, you need the sample files provided in the `inputs` folder, 
of you may forward the script to any other directory where your own inputs reside.

## Calling 

To run the script, you simply need to execute the following in the command line

```
bash parallel-do.sh
```
The on-screen messages tell you which job is exactly being executed.

## Variable definitions

In order to customize using this script, you need to adapt the paths and definitions in the preamble of the script. 
Below, I mention the meaning of the relevant variables for clarification, despite their names are self explanatory.

  * **```export reference_namelist=reference-gyre.nmlst```**

    The `reference_namelist` is the basic inlist file that is iteratively copied to the `dir_namelist` folder. This basic namelist is currently adapted to GYRE version 4.4. In the current implementation, all parameters in the inlist stay fixed, except the full path to the input file, and the full path to the output file, which are internally adapted.

  * **```export dir_inputs=inputs```**
   
    GYRE requires the input GYRE-compatible files. The directory where all input files reside are passed as `dir_inputs`.

  * **```export dir_outputs=outputs```**
   
    As GYRE keeps computing for every input model, the outputs are directed to the `dir_outputs` folder for later post-processing and analysis. If this path does not exist, it is internally created.

  * **```export dir_namelist=namelists```**

    A working copy of the `reference_namelist` is stored in `dir_namelist` and the input/output paths are adapted afterwards. This folder is automatically created if non-existent.

  * **```export namelist_prefix=namelist```**

    All working namelists inside `dir_namelist` will have identical names which begin with `namelist_prefix`, superceded with a running-counter. 

  * **```export namelist_suffix=txt```**

    All working namelists will have identical suffix defined by `namelist_suffix`. As a result, a typical namelist of the job number NNN will have the following name: `$dir_namelist/$namelist_prefix-NNN.$namelist_suffix`. Note that the suffix should be devoid of the dot "." seperator. 

  * **```export max_simultan_proc=2```**

    You can specify the *maximum* number of cores (threads) to use simultaneously. You cannot exceed the number of available cores on your computing node. If this occurs by mistake, the script corrects for it, by setting the `max_simultan_proc` to the number of available cores on the machine.

  * **```export OMP_NUM_THREADS=1```**

    Keep this always untouched, unless you know what you're doing.

  * **```export gyre=$GYRE_DIR/bin/gyre_ad```**

    The full path to the GYRE executable (versions <= 4.4) can be specified accordingly. One can choose to use either `gyre_ad` or `gyre_nad` for adiabatic or non-adiabatic computations, respectively.

## Inputs

The `inputs` folder provides seven MESA input models of a 6 solar mass massive main sequence star
at seven instances during its evolution, when hydrogen content in its core (Xc) drops by
10%, i.e. Xc=0.70, 0.60, ..., 0.10. The filenames give a clear idea of the parameters used to generate
these inputs.

## Adapting to your problem

There are two possibilities:
1. You either stick to using GYRE, and all you need to modify in the script is the input/output directories
   in the preamble of the script.
2. You would like to use a parallel do for calling a totally different executable. In this case, you need to 
   extend the scope of this script to your problem (defining new functions), keeping in mind the general 
   workflow of the current




